module bitbucket.org/alima123/checksum

go 1.13

require (
	bitbucket.org/alima123/cachepool v0.0.0-20191227155318-5b546e880951
	bitbucket.org/alima123/util v0.0.2
	github.com/json-iterator/go v1.1.9
)
