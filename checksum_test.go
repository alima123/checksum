package checksum

import (
	"fmt"
	"log"
	"testing"

	"bitbucket.org/alima123/util"
)

var (
	flagList = []int{
		CHECKSUM_MD5 | 000000000000000000000000000 | 00000000000000000000000,
		000000000000000000000 | CHECKSUM_SLICE_MD5 | 00000000000000000000000,
		000000000000000000000 | 000000000000000000000000000 | CHECKSUM_CRC32,
		CHECKSUM_MD5 | CHECKSUM_SLICE_MD5 | 00000000000000000000000,
		000000000000000000000 | CHECKSUM_SLICE_MD5 | CHECKSUM_CRC32,
		CHECKSUM_MD5 | 000000000000000000000000000 | CHECKSUM_CRC32,
		CHECKSUM_MD5 | CHECKSUM_SLICE_MD5 | CHECKSUM_CRC32,
	}
)

func printFileMeta(meta *LocalFileMeta) {
	fmt.Printf("length: %d, slicemd5: %x, md5: %x, crc32: %d, modtime: %d \n", meta.Length, meta.SliceMD5, meta.MD5, meta.CRC32, meta.ModTime)
}

func nTestChecksum(t *testing.T) {
	fmt.Println("--- file.go")
	for _, flag := range flagList {
		lf, err := GetFileSum("file.go", flag)
		if err != nil {
			t.Fatal(err)
		}
		printFileMeta(&lf.LocalFileMeta)
	}

	fmt.Println("--- file")
	for _, flag := range flagList {
		lf := NewLocalFileChecksumWithBufSize("file", DefaultBufSize-3, DefaultBufSize)
		err := lf.OpenPath()
		if err != nil {
			t.Fatal(err)
		}
		lf.Sum(flag)
		printFileMeta(&lf.LocalFileMeta)
	}
}

func TestCheckPrint(t *testing.T) {
	readPath := "file"
	files, err := util.WalkDir(readPath, ".go")
	if err != nil {
		log.Fatal(err)
	}
	for k, filePath := range files {
		lp, err := GetFileSum(filePath, CHECKSUM_MD5|CHECKSUM_SLICE_MD5|CHECKSUM_CRC32)
		if err != nil {
			fmt.Printf("[%d] %s\n", k+1, err)
			continue
		}
		fmt.Printf("[%d] - [%s]:\n", k+1, filePath)
		printFileMeta(&lp.LocalFileMeta)
	}
}
